<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit168e5c32b80c634ffdcb55d76ff41c14
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP143493',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit168e5c32b80c634ffdcb55d76ff41c14::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit168e5c32b80c634ffdcb55d76ff41c14::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
